package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string)
	go greet(ex)
	ex <- "ikhsan"
	fmt.Println("done")
}

func greet(input chan string){
	fmt.Println("greet "+ <-input +"")
}