package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string, 2)
	go greetingsss(ex)
	ex <- "ikhsan"
	ex <- "ariya"
	ex <- "girinata"
	fmt.Println(len(ex))
	fmt.Println("done")
}

func greetingsss(input chan string){
	fmt.Println("greet "+ <-input +"")
	fmt.Println("greet "+ <-input +"")

}