package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string)
	go call(ex)
	for val := range ex{
		fmt.Println(val)
	}
	fmt.Println("done")
}

func call(input chan string){

	input <- "ikhsan"
	input <- "ariya"
	input <- "girinata"
	close(input)
}