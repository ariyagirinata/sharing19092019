package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string)
	go greeting(ex)
	ex <- "ikhsan"
	close(ex)
	ex <- "ariya"
	fmt.Println("done")
}

func greeting(input chan string){
	fmt.Println("greet "+ <-input +"")
}