package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string, 2)
	go greetingss(ex)
	ex <- "ikhsan"
	ex <- "ariya"
	fmt.Println(len(ex))
	fmt.Println("done")
}

func greetingss(input chan string){
	fmt.Println("greet "+ <-input +"")
	fmt.Println("greet "+ <-input +"")

}