package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string)
	greets(ex)
	ex <- "ikhsan"
	fmt.Println("done")
}

func greets(input chan string){
	fmt.Println("greet "+ <-input +"")
}