package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type User struct {
	Id    int     `json:"id"`
	Name  string  `json:"name"`
	Email string  `json:"email"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Workshop visionet")
}

func jsonHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	user := User {
		Id: 1,
		Name: "Ikhsan Ariya",
		Email: "ariya@gmail.com",
	}
	json.NewEncoder(w).Encode(user)
}

func jsonArrayHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	user := User {
		Id: 1,
		Name: "Ikhsan Ariya",
		Email: "ariya@gmail.com",
	}
	users := []User{}
	users = append(users, user)
	json.NewEncoder(w).Encode(users)
}

func main() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/json", jsonHandler)
	http.HandleFunc("/jsonArray", jsonArrayHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}