package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("before")
	go printRout()
	time.Sleep(time.Second*4)
	fmt.Println("after")
}

func printRout(){
	time.Sleep(time.Second*5)
	fmt.Println("goroutine")
}