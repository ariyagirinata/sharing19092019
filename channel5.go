package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string)
	go greetings(ex)
	ex <- "ikhsan"
	ex <- "ariya"
	fmt.Println("done")
}

func greetings(input chan string){
	fmt.Println("greet "+ <-input +"")
}