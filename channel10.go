package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string, 2)
	go greetss(ex)
	ex <- "ikhsan"
	ex <- "ariya"
	ex <- "girinata"
	fmt.Println(len(ex))
	fmt.Println("done")
}

func greetss(input chan string){
	for val := range input{
		fmt.Println(val)
	}

}