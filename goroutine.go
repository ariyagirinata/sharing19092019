package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("before")
	go print()
	fmt.Println("after")
}

func print(){
	time.Sleep(time.Second*5)
	fmt.Println("goroutine")
}