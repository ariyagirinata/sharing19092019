package main

import "fmt"

func main() {
	fmt.Println("start")
	ex := make(chan string)
	go calls(ex)
	ex <- "ikhsan"
	ex <- "ariya"
	ex <- "girinata"
	close(ex)
	fmt.Println("done")
}

func calls(input chan string){
	for val := range input{
		fmt.Println(val)
	}

}