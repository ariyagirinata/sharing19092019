package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("before")
	go prints()
	time.Sleep(time.Second*6)
	fmt.Println("after")
}

func prints(){
	time.Sleep(time.Second*5)
	fmt.Println("goroutine")
}