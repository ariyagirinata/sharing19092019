package main

import (
	"fmt"
	"goroutine/db"
)

func main() {
	const key  ="name"
	dbInstance := db.Implement{}
	name := dbInstance.GetName(key)
	fmt.Println(name)
	//redisClient := redis.NewClient(&redis.Options{
	//	Addr:     "127.0.0.1:6379",
	//	Password: "", // no password set
	//	DB:       0,  // use default DB
	//})
	//_, err := redisClient.Ping().Result()
	//if err!=nil{
	//	log.Fatal(err.Error())
	//}
	//cache := redisClient.Get(key).Val()
	//if cache==""{
	//	name = dbInstance.GetName(key)
	//	redisClient.Set(key, name,0)
	//}else{
	//	fmt.Println("get from cache")
	//	name = cache
	//}
	//fmt.Println(name)
}
